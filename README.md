# Artificial intelligence for intelligent Codemillers
_Codemill august 31, 2018_
## Preparations
* Have Python 3 installed
* Create a Virtualenv with Python 3 _(or use Conda if you prefer)_
* Optional: GPU support
    * CUDA Toolkit 9.0; follow the guide:
    https://www.tensorflow.org/install/install_linux#NVIDIARequirements
    * _(if installing CUDA fails, train your nets on CPU, should be manageable)_
* Tensorflow according to https://www.tensorflow.org/install/ (in best case, just `pip install tensorflow-gpu`)
* `pip install jupyter  # for interactive python sessions in the browser`
* `pip install keras  # for neural nets`
* `pip install matplotlib  # visualizing results`
* `pip install gensim scikit-learn  # classification toolboxes`
* `wget http://titan.codemill.se/\~emilun/news-texts.tgz` and unpack it into the top-level dir of this repo

--------
