import html
import json
import os
import random
import re
from gensim.parsing.preprocessing import preprocess_string, strip_multiple_whitespaces, strip_tags, strip_short, \
    strip_non_alphanum
from text.stopwords import get_stopwords


STOPWORDS = get_stopwords()

def swedish_stopword_filter(s):
    return " ".join(w for w in s.split() if w not in STOPWORDS)


FILTERS = [strip_multiple_whitespaces, strip_tags, strip_non_alphanum]  # , strip_short, swedish_stopword_filter]


class NewsTextReader(object):
    def __init__(self, textdir):
        self.textdir = textdir

    def __iter__(self):
        for infilename in os.listdir(self.textdir):
            yield _get_doc(self.textdir, infilename)


def _get_doc(textdir, filename, get_bigrams=False):
    article_filepath = os.path.join(textdir, filename)
    try:
        with open(article_filepath, 'rb') as infile:
            article = infile.read()

        article = article.decode('utf-8')

        article = json.loads(article)
        body = article['body']
        body = html.unescape(body)
        text = '\n'.join((article['title'], article['preamble'], body))
        if get_bigrams:
            text = re.sub(r'[^\w\s]', '', text)
            words = text.lower().split()
            return list(' '.join(words[i:i+2]) for i in range(len(words)-1))
        else:
            return preprocess_string(text.lower(), filters=FILTERS)
    except Exception as e:
        print(e)
        return []
